# ShopMatcher
This repository contains code and resources for the
[WirVsVirus Hackathon](https://wirvsvirushackathon.org/).

## Description
ShopMatcher is a Tinder-like app to shop for yourself and for your neighbours in order to minimize 
the risk of infection. 
Using the app registered users can indicate that they 
a) either want to make a purchase or 
b) need basic food (such as flour).
The goal is that shoppers bring food from the supermarket to the searcher.
In this way, fewer people should go shopping and the risk of infection for COVID-19
should be minimised.

## Use Cases
Please see [user journey](documentation/use_case/User_Journey.png) for further information.

## Data Model
Please see [data stream](documentation/data_stream/Data_Stream.png) for further information.