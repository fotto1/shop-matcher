import React from 'react';
import './App.css';
import Login from './Login';
import ShoppingList from './Shoppinglist';
import {useAuth0} from './react-auth0-spa';

import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

function Cart(props) {
  return <pre>{JSON.stringify(props, null, 2)}</pre>;
}


function App() {
  const { isAuthenticated, logout } = useAuth0();
  return (
    <div className="App">
      <header className="App-header">
        <h1>Shop Matcher</h1>
      </header>
      <Router>
        {isAuthenticated && <button type="button" onClick={() => {      logout()}} >Logout</button>}
        <nav >
          <ul>
            <li>
              <Link to="/">Root</Link>
            </li>
            <li>
              <Link to="/shop/cart">Cart</Link>
            </li>
            <li>
              <Link to="/shop/list">My List</Link>
            </li>
            <li>
              <Link to="/shop/cart/foobar">Ich gehe einkaufen (checkin)</Link>
            </li>
          </ul>
        </nav>
        <main>
          <Switch>
            <Route path="/shop/cart" component={Cart} exact={true} />
            <Route path="/shop/cart/:id" component={Cart} />
            <Route path="/shop/list" component={ShoppingList} />
            <Login></Login>
          </Switch>
        </main>
      </Router>
    </div>
  );
}

export default App;
