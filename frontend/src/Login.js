import React from "react";
import { useAuth0 } from "./react-auth0-spa";

export default function Login(props) {
  const auth = useAuth0();
  const [token, setToken] = React.useState("token");

  React.useEffect(() => {
    if (!auth) {
      return;
    }
    if (auth.loading === true) {
      return;
    }

    if (auth.isAuthenticated === false) {
      return;
    }

    auth.getTokenSilently().then(x => setToken(x));
  }, [auth]);

  if (auth.loading) {
    return <p>Please wait...</p>;
  }

  if (auth.isAuthenticated !== true) {
    return (
      <div>
        <button onClick={e => auth.loginWithRedirect({})}>Auth0</button>
      </div>
    );
  }

  return (
    <React.Fragment>
      <h2>Hallo, {auth.user.name}!</h2>
      <details>
        <summary>Token</summary>
        {token}
      </details>
    </React.Fragment>
  );
}
