import React from "react";

class ShoppingList extends React.Component {
  render() {
    return (
      <div>
        <h1>Your ShoppingList</h1>
        <label for="goods">Fügen sie etwas zu ihrer Einkaufsliste hinzu</label>
        <select id="goods" name="goods">
          <option value="apfel">Apfel</option>
          <option value="mehl">Mehl</option>
          <option value="eier">Eier</option>
          <option value="test">Test</option>
        </select>
        <button>add</button>
        <ol>
          <li>good1 500g</li>
          <li>good2 500g</li>
          <li>good3 500g</li>
          <li>good4 500g</li>
        </ol>
        <h2>zeitpunkt zum Einkaufen auswählen</h2>
        <input type="datetime-local"></input>
        <button>Einakufsliste speichern</button>
        <button>Einkaufsangebot senden</button>
      </div>
    );
  }
}

export default ShoppingList;
